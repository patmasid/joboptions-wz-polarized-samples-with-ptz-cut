```
cd runFiles
python run.py
cd <directory created for the run using run.py>
source run.sh
```

You can change the Athena version in the file ```run.py```.

Also, these jobOptions include Pythia8_Process = "guess" option as there was some problem with the Pythia version we were using for Ath 21.6.58. See the JIRA tickets https://its.cern.ch/jira/browse/AGENE-1959 and https://its.cern.ch/jira/browse/AGENE-1970.