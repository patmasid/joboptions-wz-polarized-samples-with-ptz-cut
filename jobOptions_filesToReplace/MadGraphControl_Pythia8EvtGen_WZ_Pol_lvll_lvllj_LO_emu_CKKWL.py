from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

mode=0

nevents = 10000
ktdurham = 25.0
dparameter = 0.4
maxjetflavor = 4

name = pol_state+"_lvll_lvllj_LO_emu_"+PtZRegion

proc_str = ""

if(pol_state=="W0Z0"):
    proc_str = "wpm{0} z{0}"
elif(pol_state=="W0ZT"):
    proc_str = "wpm{0} z{T}"
elif(pol_state=="WTZ0"):
    proc_str = "wpm{T} z{0}"
elif(pol_state=="WTZT"):
    proc_str = "wpm{T} z{T}"
elif(pol_state=="WZIncl"):
    proc_str = "wpm z"
else:
    print("ERROR: pol_state has to be one of W0Z0, W0ZT, WTZ0, WTZT, WZIncl ")

process  = """
import model sm
define wpm = w+ w-
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm
define vl~ = ve~ vm~
define wpm = w+ w-
generate p p > %s, w+ > l+ vl, w- > l- vl~, z > l+ l- @0
add process p p > %s j, w+ > l+ vl, w- > l- vl~, z > l+ l- @1
output -f
""" % (proc_str, proc_str)

process_dir = new_process(process)

#Fetch default LO run_card.dat and set parameters                                                                                                

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

#----------------------------------------------------------------------------
# Random Seed
#----------------------------------------------------------------------------                                                                     
randomSeed = 0
if hasattr(runArgs,'randomSeed'): randomSeed = runArgs.randomSeed

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------                                                                     
safefactor = 2.0
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  nevents = int(runArgs.maxEvents)*safefactor
else: nevents = nevents*safefactor

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents

extras = { 'me_frame' : '3,4,5,6',
           'lhe_version' : '3.0',
           'cut_decays' : 'True',
           'clusinfo' : 'T',
           'auto_ptj_mjj' : 'False',
           'maxjetflavor' : 4,
           'asrwgtflavor' : 4,
           'ickkw' : 0,
           'xqcut' : 0.0,
           'drjl' : 0.0,
           'drll' : 0.0,
           'etal' : -1.0,
           'etaj' : -1.0,
           'ptl' : 0.0,
           'ptj' : ktdurham,
           'ktdurham' : 25.0,
           'dparameter' : 0.4,
           'nevents'      : nevents,
}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

# copy local version of dummy_fct.f
if(PtZRegion!="PtZIncl"):                                                                                    
    file_loc = ""
    if(PtZRegion=="PtZge150GeV"):
        file_loc='dummy_fct_PtZge150GeV.f'
    elif(PtZRegion=="PtZlt150GeV"):
        file_loc='dummy_fct_PtZlt150GeV.f'
    else:
        print("ERROR: PtZRegion should be one of the 'PtZge150GeV' or 'PtZlt150GeV'.")
        
    if not os.access(file_loc,os.R_OK):
        RuntimeError('Could not find '+str(file_loc))

    shutil.copy(file_loc,process_dir+"/SubProcesses/dummy_fct.f")

print_cards()

generate(process_dir=process_dir,runArgs=runArgs)

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

############################                                                                                                
# Shower JOs will go here                                                                                                             

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

evgenConfig.generators = ["MadGraph","Pythia8","EvtGen"]
evgenConfig.description = 'MadGraph_'+str(name)
evgenConfig.contact = [ 'Prachi Arvind Atmasiddha <prachi.atmasiddha@cern.ch>' ]
evgenConfig.keywords+=["inclusive","WZ","LO"]

nJetMax = 1

PYTHIA8_TMS=ktdurham
PYTHIA8_nJetMax=nJetMax
PYTHIA8_Dparameter=dparameter
PYTHIA8_Process= "guess"
PYTHIA8_nQuarksMerge=maxjetflavor
genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']
include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")
