#!/usr/bin/python

import sys,string,random,os,fileinput

def createRunDir():
    
    if(len(sys.argv)==3):
        
        pol_state = sys.argv[1]
        PtZRegion = sys.argv[2]

        run_dir = ""
        control_jobOption_py = ""
        jobOption_py = ""
        dummy_fct_file = ""
        
        run_dir = "../"+pol_state+"_"+PtZRegion
        control_jobOption_py = "../jobOptions_filesToReplace/MadGraphControl_Pythia8EvtGen_WZ_Pol_lvll_lvllj_LO_emu.py"
        jobOption_py = "../jobOptions_filesToReplace/mc.MGPy8EG_"+pol_state+"_lvll_lvllj_LO_emu_"+PtZRegion+".py"
        dummy_fct_file = "../jobOptions_filesToReplace/dummy_fct_"+PtZRegion+".f"

        os.mkdir("../"+pol_state+"_"+PtZRegion)
        os.chdir("../"+pol_state+"_"+PtZRegion)
        
        os.system("cp "+control_jobOption_py+" ./")
        os.system("cp "+jobOption_py+" ./")
        os.system("cp "+dummy_fct_file+" ./")
        
        runfile_path = run_dir+"/run.sh"
        runFile = open(runfile_path, 'w')
        
        runFile.write("#!/bin/sh\n")
        runFile.write("cd "+run_dir+"\n")
        runFile.write("setupATLAS\n")
        runFile.write("asetup 21.6.58,AthGeneration,here\n")
        runFile.write("Gen_tf.py --ecmEnergy=13000. --maxEvents=10000 --runNumber=999999 --firstEvent=1 --randomSeed=0" + " --outputEVNTFile=EVNT.root"+" --jobConfig="+run_dir+"\n")
        
        os.system("chmod +x run.sh")
        
if(__name__=="__main__"):
    createRunDir()
